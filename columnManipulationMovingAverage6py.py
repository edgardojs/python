import pandas as pd
import pandas_datareader.data as web
import datetime
import matplotlib.pyplot as plt
from matplotlib import style
import numpy as np
from statistics import mean

style.use('fivethirtyeight')

def bytespdate2num(fmt, encoding = 'utf-8'):
    str_converter = mdates.strpdate2num(fmt)
    def bytes_converter(b):
        s = b.decode(encoding)
        return str_converter(s)
    return bytes_converter

def high_minus_low(highs,lows):
    return highs-lows


def moving_average(data):
    return mean(data)

def fancy_this(data):
    return mean(data)+ mean(data) * .1618
def fancy_this2(data):
    return mean(data)+ mean(data) * -.1618

start = datetime.datetime(2006,1,1)
end = datetime.datetime(2016,10,25)
att = web.DataReader("T", 'yahoo', start, end)
describe = att.describe()


att['50MA'] = pd.Series.rolling(att['Close'],50).mean()
att['10MA'] = pd.Series.rolling(att['Close'],10).mean()

fig, axes = plt.subplots(nrows = 3, ncols=1,sharex=True)


att['50STD'] = pd.Series.rolling(att['Close'],50).std()
att['50STD'].plot(ax=axes[0], label='50STD',linewidth=1)
att['10STD'] = pd.Series.rolling(att['Close'],10).std()
att['10STD'].plot(ax=axes[0], label='50STD',linewidth=1)

plt.setp(att['Close'])
att['Close'].plot(ax=axes[1], label='Price')
att['50MA'].plot(ax=axes[1],label='50MA')
att['10MA'].plot(ax=axes[1],label= '10MA')
att['Open'].plot(ax=axes[1],label='Open')

att['apply'] = pd.Series.rolling(att['Open'],50).apply(func=fancy_this)
att['apply'].plot(ax=axes[2],label= 'open')
att['apply'] = pd.Series.rolling(att['Open'],50).apply(func=fancy_this2)
att['apply'].plot(ax=axes[2],label= 'open2')
att['MA_with_apply'] = pd.Series.rolling(att['Close'],50).apply(func=moving_average)
att['MA_with_apply'].plot(ax=axes[2], label = 'MA_with_apply')
print(att.head())

#att.dropna(how='all',inplace=True)
#att.dropna(inplace=True)
#att.dropna(how='all',inplace=True)

#att.fillna(method='ffill',inplace=True)
#att.fillna(method='bfill',inplace=True)

one_pct = len(att)*0.01

att.fillna(value=-99999,inplace=True, limit=one_pct)
print(att)
if att.isnull().values.sum() > 1:
    print('Found remaining NA, more than 1% is not a number')


att.fillna(value=-99999,inplace=True, limit=9000)
if att.isnull().values.sum() > 1:
    print('Found remaining NA, more than 1%+900 is not a number')

plt.show()
