import pandas as pd
from pandas import Series
import urllib.request
import urllib
import matplotlib.dates as mdates
import numpy as np
import math
import matplotlib.pyplot as plt
from matplotlib import style
from matplotlib.finance import candlestick_ohlc
import matplotlib.ticker as mticker
from scipy import stats

style.use('bmh')

MA1 = 14
MA2 = 28

def bytespdate2num(fmt, encoding='utf-8'):
    str_converter = mdates.strpdate2num(fmt)
    def bytes_converter(b):
        s = b.decode(encoding)
        return str_converter(s)
    return bytes_converter

def high_minus_low(highs, lows):
    return highs-lows

def moving_average(values, window):
    weights = np.repeat(1.0, window) / window
    smas = np.convolve(values, weights, 'valid')
    return smas

def graph_stock(stock):

    fig = plt.figure()
    ax1 = plt.subplot2grid((6,1),(0,0),rowspan=1, colspan=1)
    ax2 = plt.subplot2grid((6,1),(1,0),rowspan=4, colspan=1)
    plt.ylabel('Price')
    ax3 = plt.subplot2grid((6,1), (5,0),rowspan=1, colspan=1)

    
    print("Currently polling:", stock)
    url = 'http://chartapi.finance.yahoo.com/instrument/1.0/'+stock+'/chartdata;type=quote;range=6m/csv'
    source_code = urllib.request.urlopen(url).read().decode()
    stock_data = []
    split_source = source_code.split('\n')

    for each_line in split_source:
        split_line = each_line.split(',')
        if len(split_line) == 6:
            if 'values' not in each_line:
                stock_data.append(each_line)

    date, closep, highp, lowp, openp, volume = np.loadtxt(stock_data, delimiter = ',',
                                                          unpack = True,
                                                          converters = {0: bytespdate2num('%Y%m%d')})

    ma1 = moving_average(closep, MA1)
    ma2 = moving_average(closep, MA2)
    start = len(date[MA2-1:])
    
    df = Series([closep,openp,lowp,highp,volume],index=["Closep","Openp","Lowp","Highp","Volume"],dtype=np.float64)
    df.fillna(-99999,inplace=True)
    df['HL_PCT'] = (df['Highp'] - df['Closep']) / df['Closep'] * 100.00
    df['PCT_change'] = (df['Closep'] - df['Openp']) / df['Openp'] * 100.00
    df['MA1'] = ma1
    df['MA2'] = ma2
    df['H_L'] = df['Highp']-df['Lowp']

    
    
    x = 0
    y = len(date)

    xs = np.array(date)
    ys = np.array(df['Closep'])
    

    slope, intercept, r_value, p_value, std_err = stats.linregress(xs,ys)

    df2 = Series([slope, intercept, r_value, p_value, std_err],index=["Slope","Intercept","R_value","P_value","Std_err"],dtype=np.float64)
    df2.fillna(-99999, inplace=True)
    
    
    new_list= []
    while x < y:
        append_line =  date[x], openp[x], highp[x], lowp[x], closep[x], volume[x]
        new_list.append(append_line)
        x += 1

    h_l = list(map(high_minus_low,highp,lowp))

    print(df2)
    
    ax1.plot(date,h_l)
        

    candlestick_ohlc(ax2, new_list, width = 0.6, colorup = '#41ad49',colordown = '#ff1717')
    ax2.grid(True, color = 'g', linestyle = '-', linewidth = 0.2)
    for label in ax2.xaxis.get_ticklabels():
        label.set_rotation(45)
    
    ax2.xaxis.set_major_locator(mticker.MaxNLocator(10))
    ax2.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    
    bbox_props = dict(boxstyle = 'round4 , pad =0.3', fc = "#f2f1f1", ec = 'k', lw = 2 )
    ax2.annotate(str(closep[-1]), (date[-1], closep[-1]),
                 xytext = (date[-1]+5, closep[-1]), bbox = bbox_props)


    print(len(date))
    print(len(ma1))
    ax2.plot(date[-start:], ma1[-start:])
    ax2.plot(date[-start:], ma2[-start:])

    ax3.plot(date, df['HL_PCT'])
    ax3.plot(date, df['PCT_change'])
             
    
    plt.subplots_adjust(left = 0.09, bottom = 0.16, right = 0.94, top = 0.95, wspace = 0.2, hspace = 0.2)
    plt.show()

    

stock = input('Stock to plot: ')
graph_stock(stock)


